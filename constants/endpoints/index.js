const endpoints = {
  POST_REGISTER: 'customers',
  GET_LOGIN: 'login/customer',
  GET_PRODUCTS: 'cloths',
  GET_CATEGORIES: 'category/mainCategory',
  GET_PRODUCTS_BY_CATEGORY: 'products/category',
  POST_ORDER: 'orders',
  GET_ORDERS: 'orders/customer',
  GET_JEWELLERY: 'jewellerys',
  GET_JEWELLERY_BY_CATEGORY: 'jewellerys/category',
};

export default endpoints;
